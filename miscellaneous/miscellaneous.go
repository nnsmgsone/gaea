package miscellaneous

func Max(x, y int) int {
	switch {
	case x < y:
		return y
	default:
		return x
	}
}

func Min(x, y int) int {
	switch {
	case x < y:
		return x
	default:
		return y
	}
}
