package treeMap

import (
	"fmt"
	"math/rand"
	"testing"

	"gitlab.com/nnsmgsone/gaea/curry"
	"gitlab.com/nnsmgsone/gaea/typeclass"
)

func lPrint(x typeclass.Ord, n interface{}) interface{} {
	i, _ := n.(int)
	fmt.Printf("\t%d: %v\n", i, x)
	return i + 1
}

func rPrint(x typeclass.Ord, n interface{}) interface{} {
	i, _ := n.(int)
	fmt.Printf("\t%d: %v\n", i, x)
	return i - 1
}

func Test(t *testing.T) {
	m := New()

	for i := 0; i < 100; i++ {
		m.Insert(typeclass.NewInt(rand.Intn(1000)), i+1)
	}

	fmt.Printf("Init:\n")
	m.Foldl(lPrint, 0)

	fmt.Printf("Length: %v\n", m.Length())
	fmt.Printf("Max: %v\n", m.Maximum())
	fmt.Printf("Min: %v\n", m.Minimum())
	fmt.Printf("isEmpty: %v\n", m.Null())

	v := typeclass.NewInt(828)
	fmt.Printf("%v is Exist: %v\n", v, m.Elem(v))

	v = typeclass.NewInt(829)
	fmt.Printf("%v is Exist: %v\n", v, m.Elem(v))

	v = typeclass.NewInt(828)
	m.Delete(v)

	fmt.Printf("Delete 828:\n")
	m.Foldr(rPrint, m.Length()-1)

	m0 := m.Filter(curry.Lt(NewNode(typeclass.NewInt(200), 0)))

	fmt.Printf("Filter < 200:\n")
	m0.Foldl(lPrint, 0)
}
