package treeMap

import (
	"gitlab.com/nnsmgsone/gaea/functor"
	"gitlab.com/nnsmgsone/gaea/rbtree"
	"gitlab.com/nnsmgsone/gaea/typeclass"
)

type TreeMap interface {
	Clear()
	Delete(typeclass.Ord) error
	Insert(typeclass.Ord, interface{}) error
	Filter(func(typeclass.Ord) bool) TreeMap
	functor.Functor
}

type TreeMapNode interface {
	typeclass.Ord
	V() interface{}
	K() typeclass.Ord
}

type treeMap struct {
	tree rbtree.RBtree
}

type treeMapNode struct {
	v interface{}
	k typeclass.Ord
}

func (a *treeMapNode) V() interface{} {
	return a.v
}

func (a *treeMapNode) K() typeclass.Ord {
	return a.k
}

func (a *treeMapNode) Eq(b typeclass.Eq) bool {
	v, _ := b.(*treeMapNode)
	return a.k.Eq(v.k)
}

func (a *treeMapNode) NotEq(b typeclass.Eq) bool {
	return !a.Eq(b)
}

func (a *treeMapNode) Lt(b typeclass.Ord) bool {
	v, _ := b.(*treeMapNode)
	return a.k.Lt(v.k)
}

func (a *treeMapNode) Le(b typeclass.Ord) bool {
	v, _ := b.(*treeMapNode)
	return a.k.Le(v.k)
}

func (a *treeMapNode) Gt(b typeclass.Ord) bool {
	v, _ := b.(*treeMapNode)
	return a.k.Gt(v.k)
}

func (a *treeMapNode) Ge(b typeclass.Ord) bool {
	v, _ := b.(*treeMapNode)
	return a.k.Ge(v.k)
}

func (a *treeMapNode) Compare(b typeclass.Ord) typeclass.Ordering {
	switch {
	case a.Lt(b):
		return typeclass.LT
	case a.Eq(b):
		return typeclass.EQ
	default:
		return typeclass.GT
	}
}

func (m *treeMap) Clear() {
	m.tree.Clear()
}

func (m *treeMap) Delete(k typeclass.Ord) error {
	return m.tree.Delete(&treeMapNode{k: k})
}

func (m *treeMap) Insert(k typeclass.Ord, v interface{}) error {
	if a := m.Elem(k); a != nil {
		b, _ := a.(*treeMapNode)
		b.v = v
		return nil
	}
	return m.tree.Insert(&treeMapNode{k: k, v: v})
}

func (m *treeMap) Filter(f func(typeclass.Ord) bool) TreeMap {
	return &treeMap{m.tree.Filter(f)}
}

func (m *treeMap) Null() bool {
	return m.tree.Null()
}

func (m *treeMap) Length() int {
	return m.tree.Length()
}

func (m *treeMap) Minimum() typeclass.Ord {
	return m.tree.Minimum()
}

func (m *treeMap) Maximum() typeclass.Ord {
	return m.tree.Maximum()
}

func (m *treeMap) Elem(k typeclass.Ord) typeclass.Ord {
	return m.tree.Elem(&treeMapNode{k: k})
}

func (m *treeMap) Map(f functor.MapFunc) functor.Functor {
	return m.tree.Map(f)
}

func (m *treeMap) Foldl(f functor.FoldFunc, b interface{}) interface{} {
	return m.tree.Foldl(f, b)
}

func (m *treeMap) Foldr(f functor.FoldFunc, b interface{}) interface{} {
	return m.tree.Foldr(f, b)
}
