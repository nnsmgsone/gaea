package treeMap

import (
	"gitlab.com/nnsmgsone/gaea/rbtree"
	"gitlab.com/nnsmgsone/gaea/typeclass"
)

func New() TreeMap {
	return &treeMap{rbtree.New()}
}

func NewNode(k typeclass.Ord, v interface{}) TreeMapNode {
	return &treeMapNode{k: k, v: v}
}
